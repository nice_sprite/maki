use anyhow;
use cpal::{
    traits::{DeviceTrait, HostTrait, StreamTrait},
    FromSample, Sample, SizedSample, Stream,
};
use lofty::AudioFile;
use log::{debug, error, info, warn};

use rodio::Source;
use std::{
    path::PathBuf,
    sync::{mpsc::*, Mutex},
};
use std::{sync::Arc, time::Duration};

pub struct AudioPlayer {
    host: Arc<cpal::Host>,
    device: Arc<cpal::Device>,
    config: Arc<cpal::SupportedStreamConfig>,
    control_sender: Option<Sender<PlayerCommand>>,
    song_list: Vec<String>,
    current_song: String,
    current_song_index: i32,
    current_stream: Option<Arc<Mutex<AudioStream>>>,
    audio_control_state: Arc<Mutex<AudioControls>>,
}

struct AudioTimer {
    pub last_time: std::time::Instant,

    /// real-world time, does not care about pause
    pub elapsed_real_time: std::time::Duration,

    /// audio-time, the amount of time the audio has actually been playing, respects pause.
    pub elapsed_audio_time: std::time::Duration,

    pub paused: bool,
}

impl AudioTimer {
    pub fn new() -> Self {
        Self {
            last_time: std::time::Instant::now(),
            elapsed_real_time: std::time::Duration::default(),
            elapsed_audio_time: std::time::Duration::default(),
            paused: false,
        }
    }

    pub fn pause(&mut self) {
        self.paused = true;
    }

    pub fn play(&mut self) {
        self.paused = false;
    }

    /// returns time since last `tick()`
    pub fn tick(&mut self) -> std::time::Duration {
        let now = std::time::Instant::now();
        let dur = now - self.last_time;
        self.elapsed_real_time += dur; // always update this
        if !self.paused {
            self.elapsed_audio_time += dur;
        }
        self.last_time = now;
        dur
    }

    /// skip ahead by `amount`
    pub fn skip_ahead(&mut self, amount: std::time::Duration) {
        self.elapsed_audio_time += amount;
    }
}

struct AudioStream {
    stream: Stream,
    duration: std::time::Duration,
    timer: AudioTimer,
    control_state: Arc<Mutex<AudioControls>>,
}

// shared between the UI thread and the audio thread
pub struct AudioControls {
    requested_volume_scale: f32,
    requested_seek_to_time: Option<std::time::Duration>,
}

impl Default for AudioControls {
    fn default() -> Self {
        Self {
            requested_seek_to_time: None,
            requested_volume_scale: 1.0,
        }
    }
}

/// Command packets to be sent to the audio thread from the UI thread to control the playback
pub enum PlayerCommand {
    PlaySong(std::path::PathBuf),
    Volume(f32),
    Seek(f32),
    Play,
    Pause,
}

use crate::renderer::traits::UiUpdater;

impl AudioPlayer {
    pub fn new() -> anyhow::Result<Self, anyhow::Error> {
        let host = cpal::default_host();

        let device = host.default_output_device().unwrap();
        info!("Output device: {}", device.name()?);

        let config = device.default_output_config().unwrap();
        info!("Default output config: {:?}", config);

        let mut song_list = Vec::new();
        let song_dir = std::fs::read_dir("./test_audio")?;
        for s in song_dir {
            if let Ok(d) = s {
                let _ = d.path().canonicalize().map(|p| {
                    song_list.push(p.to_str().unwrap().to_string());
                });
            }
        }

        Ok(Self {
            host: Arc::new(host),
            device: Arc::new(device),
            config: Arc::new(config),
            control_sender: None,
            song_list,
            current_song: String::new(),
            current_song_index: 0i32,
            audio_control_state: Arc::new(Mutex::new(AudioControls::default())),
            current_stream: None,
        })
    }

    pub fn play(&mut self) -> Sender<PlayerCommand> {
        let device = Arc::clone(&self.device);
        let config = Arc::clone(&self.config);
        let (send, recv) = channel::<PlayerCommand>();
        self.control_sender = Some(send.clone());
        let controls = self.audio_control_state.clone(); // TODO might need to downgrade if the
                                                         // audio thread dropping + deallocing this is an issue
        let _ = std::thread::Builder::new()
            .name("audio playback".to_string())
            .spawn(move || {
                let stream_config = config.config();
                let mut current_stream: Option<AudioStream> = None;
                while let Ok(player_command) = recv.recv() {
                    match player_command {
                        PlayerCommand::PlaySong(song) => {
                            let stream = match config.sample_format() {
                                cpal::SampleFormat::I8 => {
                                    Self::run::<i8>(&song, &controls, &device, &stream_config)
                                }
                                cpal::SampleFormat::I16 => {
                                    Self::run::<i16>(&song, &controls, &device, &stream_config)
                                }
                                cpal::SampleFormat::I32 => {
                                    Self::run::<i32>(&song, &controls, &device, &stream_config)
                                }
                                cpal::SampleFormat::I64 => {
                                    Self::run::<i64>(&song, &controls, &device, &stream_config)
                                }
                                cpal::SampleFormat::U8 => {
                                    Self::run::<u8>(&song, &controls, &device, &stream_config)
                                }
                                cpal::SampleFormat::U16 => {
                                    Self::run::<u16>(&song, &controls, &device, &stream_config)
                                }
                                cpal::SampleFormat::U32 => {
                                    Self::run::<u32>(&song, &controls, &device, &stream_config)
                                }
                                cpal::SampleFormat::U64 => {
                                    Self::run::<u64>(&song, &controls, &device, &stream_config)
                                }
                                cpal::SampleFormat::F32 => {
                                    Self::run::<f32>(&song, &controls, &device, &stream_config)
                                }
                                cpal::SampleFormat::F64 => {
                                    Self::run::<f64>(&song, &controls, &device, &stream_config)
                                }
                                sample_format => {
                                    panic!("Unsupported sample format '{sample_format}'")
                                }
                            };

                            info!("playing song {song:?}");
                            current_stream = match stream {
                                Ok(stream) => Some(stream),
                                Err(e) => {
                                    error!("{e:?}");
                                    None
                                }
                            };
                            if let Some(_cs) = current_stream.as_ref() {
                                info!("we have a stream");
                            } else {
                                info!("we do NOT have a stream???");
                            }
                        }
                        PlayerCommand::Play => {
                            info!("got play command");
                            if let Some(s) = current_stream.as_ref() {
                                s.stream.play().expect("error while playing audio");
                            }
                        }
                        PlayerCommand::Pause => {
                            info!("got pause command");
                            if let Some(s) = current_stream.as_ref() {
                                s.stream.pause().expect("error while playing audio");
                            }
                        }
                        PlayerCommand::Volume(_volume) => {
                            todo!()
                        }
                        PlayerCommand::Seek(_) => todo!(),
                    }
                }
                info!("Audio thread shutting down");
            });
        send
    }

    pub fn run<T>(
        song: &PathBuf,
        with_controls: &Arc<Mutex<AudioControls>>,
        device: &cpal::Device,
        config: &cpal::StreamConfig,
    ) -> Result<AudioStream, anyhow::Error>
    where
        T: SizedSample + FromSample<f32>,
    {
        let sample_rate = config.sample_rate.0 as f32;
        let channels = config.channels as usize;
        info!("running with samplerate={sample_rate} ");
        let audio_file = std::fs::File::open(song)?;
        let lofty = lofty::read_from_path(song)?;
        let duration = dbg!(lofty.properties().duration());
        let mut decoder = rodio::decoder::Decoder::new(std::io::BufReader::new(audio_file))?;

        info!("audio file should be playing");
        dbg!(decoder.sample_rate());
        dbg!(decoder.current_frame_len());

        // TODO add volume scale control here
        let controls = Arc::downgrade(&with_controls);
        let mut next_value = move || {
            let mut volume_scale = 1.0;
            if let Some(c) = controls.upgrade() {
                if let Ok(c) = c.try_lock() {
                    volume_scale = c.requested_volume_scale;
                }
            }

            if let Some(sample) = decoder.next() {
                return sample.to_float_sample() * volume_scale;
            }
            return 0.0;
        };

        let err_fn = |err| error!("an error occurred on stream: {}", err);

        let stream = device.build_output_stream(
            config,
            move |data: &mut [T], _info: &cpal::OutputCallbackInfo| {
                Self::write_data(data, channels, &mut next_value)
            },
            err_fn,
            None,
        )?;

        stream.play()?;
        let stream = AudioStream {
            stream,
            duration,
            control_state: Arc::clone(&with_controls),
            timer: AudioTimer::new(),
        };
        Ok(stream)
    }

    fn write_data<T>(output: &mut [T], channels: usize, next_sample: &mut dyn FnMut() -> f32)
    where
        T: Sample + FromSample<f32>,
    {
        for frame in output.chunks_mut(channels) {
            for sample in frame.iter_mut() {
                let value: T = T::from_sample(next_sample());
                *sample = value;
            }
        }
    }

    fn request_volume(&mut self, volume: f32) {}
}

impl UiUpdater for AudioPlayer {
    fn ui(&mut self, ui: &mut imgui::Ui) {
        if let Some(_) = ui.window("Audio Player Controls").begin() {
            if ui.button("play") {
                if let Some(s) = self.control_sender.as_ref() {
                    let _ = s.send(PlayerCommand::Play);
                }
            }
            if ui.button("pause") {
                if let Some(s) = self.control_sender.as_ref() {
                    let _ = s.send(PlayerCommand::Pause);
                }
            }
            if let Some(_t) = ui.begin_combo("Pick Song", &self.current_song) {
                for (idx, song) in self.song_list.iter().enumerate() {
                    if ui.selectable_config(song).build() {
                        self.current_song = song.to_string();
                        self.current_song_index = idx as i32;
                        if let Some(s) = self.control_sender.as_ref() {
                            let _ = s.send(PlayerCommand::PlaySong(std::path::PathBuf::from(
                                self.current_song.clone(),
                            )));
                        }
                    }
                }
            }
            if let Some(stream) = self.current_stream.as_ref() {
                let _ = stream.try_lock().map(|mut cs| {
                    cs.timer.tick();
                    ui.text(format!("time: {:?}", cs.timer.elapsed_audio_time.as_secs()));
                    ui.text(format!("duration: {:?}", cs.duration.as_secs()));
                });
            }
        }
    }
}
