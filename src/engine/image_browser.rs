use std::path::Path;
use std::str::FromStr;
use std::sync::mpsc::{Receiver, Sender};
use std::sync::{Arc, Mutex};
use std::{collections::BTreeMap, path::PathBuf, sync::mpsc, thread};

use image::{DynamicImage, GenericImageView, Rgba, RgbaImage};
use imgui::TextureId;
use imgui_wgpu::RawTextureConfig;
use wgpu::util::DeviceExt;

use crate::renderer::texture::Texture;
use crate::renderer::traits::UiUpdater;
use std::error::Error;

use log::{error, info, warn};

#[derive(Debug, PartialEq, Clone, Copy)]
pub enum ThumbnailSize {
    Small = 32,
    Medium = 64,
    Large = 128,
}
impl ThumbnailSize {
    fn as_u32(&self) -> u32 {
        *self as u32
    }
}

pub struct TextureAsset {
    path: PathBuf,               // where the TextureAsset was loaded from
    format: wgpu::TextureFormat, // pixel format of the texture
    width: u32,
    height: u32,
    pub id: Option<TextureId>, // the imgui id
    pixels: RgbaImage,         // the pixel data
    texture: Arc<wgpu::Texture>,
    view: Arc<wgpu::TextureView>,
}

impl TextureAsset {
    pub fn from_path(
        path: &Path,
        device: &wgpu::Device,
        queue: &wgpu::Queue,
    ) -> Result<Self, Box<dyn Error>> {
        if path.try_exists()? {
            let image = image::io::Reader::open(path)?.decode()?;
            let (w, h) = image.dimensions();
            let image_rgba = image.to_rgba8();

            let desc = &wgpu::TextureDescriptor {
                label: None,
                size: wgpu::Extent3d {
                    width: w,
                    height: h,
                    depth_or_array_layers: 1,
                },
                mip_level_count: 1,
                sample_count: 1,
                dimension: wgpu::TextureDimension::D2,
                format: wgpu::TextureFormat::Rgba8UnormSrgb,
                usage: wgpu::TextureUsages::TEXTURE_BINDING | wgpu::TextureUsages::COPY_DST,
                view_formats: &[],
            };

            let texture =
                Arc::new(device.create_texture_with_data(&queue, &desc, image_rgba.as_raw()));
            let view = Arc::new(texture.create_view(&wgpu::TextureViewDescriptor::default()));

            Ok(Self {
                path: path.to_path_buf(),
                format: wgpu::TextureFormat::Rgba8Unorm,
                pixels: image_rgba,
                width: w,
                height: h,
                id: None,
                texture,
                view,
            })
        } else {
            Err(Box::new(std::io::Error::new(
                std::io::ErrorKind::NotFound,
                format!("{:?} does not exist!", path),
            )))
        }
    }

    pub fn as_imgui_texture(
        &self,
        device: &wgpu::Device,
        imgui_backend: &imgui_wgpu::Renderer,
    ) -> imgui_wgpu::Texture {
        let t = imgui_wgpu::Texture::from_raw_parts(
            device,
            &imgui_backend,
            Arc::clone(&self.texture),
            Arc::clone(&self.view),
            None,
            Some(&RawTextureConfig {
                label: None,
                sampler_desc: wgpu::SamplerDescriptor {
                    address_mode_u: wgpu::AddressMode::ClampToEdge,
                    address_mode_v: wgpu::AddressMode::ClampToEdge,
                    address_mode_w: wgpu::AddressMode::ClampToEdge,
                    ..Default::default()
                },
            }),
            wgpu::Extent3d {
                width: self.width,
                height: self.height,
                depth_or_array_layers: 1,
            },
        );
        t
    }
}

pub struct TextureBrowser {
    images: BTreeMap<PathBuf, TextureAsset>,
    thumbnail_size: ThumbnailSize,
    job_sender: Sender<TextureAssetProcessingJob>,
    images_per_row: u32,
}

pub enum TextureAssetProcessingJob {
    BatchLoadImages(Vec<PathBuf>),
    ImageLoaded(TextureAsset),
}

impl TextureBrowser {
    pub fn new(
        device: &Arc<wgpu::Device>,
        queue: &Arc<wgpu::Queue>,
    ) -> (Self, Receiver<TextureAssetProcessingJob>) {
        let (send, recv) = std::sync::mpsc::channel::<TextureAssetProcessingJob>();
        let (t_send, t_recv) = std::sync::mpsc::channel::<TextureAssetProcessingJob>();
        let images: BTreeMap<PathBuf, TextureAsset> = BTreeMap::new();

        // it is really important that this thread does not panic!
        let upload_queue = Arc::downgrade(&queue);
        let device_ref = Arc::downgrade(&device);
        //let imgui_renderer = Arc::downgrade(&imgui_renderer);
        match std::thread::Builder::new()
            .name("Asset loading thread".to_string())
            .spawn(move || {
                while let Ok(job) = recv.recv() {
                    match job {
                        TextureAssetProcessingJob::BatchLoadImages(images) => {
                            // TODO descriptive error/fallback if the device/queue are not
                            // available
                            let device_ref = match device_ref.upgrade() {
                                Some(dev) => dev,
                                None => continue,
                            };

                            let queue_ref = match upload_queue.upgrade() {
                                Some(q) => q,
                                None => continue,
                            };

                            for image in images {
                                let texture_asset = match TextureAsset::from_path(
                                    image.as_path(),
                                    &device_ref,
                                    &queue_ref,
                                ) {
                                    Ok(asset) => asset,
                                    Err(e) => {
                                        error!("error loading {image:?}: {:?}", e);
                                        continue;
                                    }
                                };
                                let _ = t_send
                                    .send(TextureAssetProcessingJob::ImageLoaded(texture_asset));
                            }
                        }
                        TextureAssetProcessingJob::ImageLoaded(_) => (),
                    }
                }
                dbg!("No more jobs can be submitted! Closing job thread");
            }) {
            Ok(_) => info!("spawned asset loading thread!"),
            Err(e) => panic!("failed to create asset loading thread! {:?}", e),
        }
        (
            Self {
                images,
                thumbnail_size: ThumbnailSize::Small,
                job_sender: send,
                images_per_row: 8,
            },
            t_recv,
        )
    }

    pub fn add_image(&mut self, texture: TextureAsset) {
        self.images.insert(texture.path.to_path_buf(), texture);
    }

    pub fn pretty_path(asset_path: &PathBuf) -> &str {
        asset_path
            .file_name()
            .expect("tried to prettify a path that had no filename!")
            .to_str()
            .unwrap()
    }
}

impl UiUpdater for TextureBrowser {
    fn ui(&mut self, ui: &mut imgui::Ui) {
        ui.window("browser").build(|| {
            if ui.button("add asset folder") {
                if let Some(files) = rfd::FileDialog::new()
                    .add_filter("images", &["png", "tiff", "jpg", "tga", "psd"])
                    .pick_files()
                {
                    let _ = self
                        .job_sender
                        .send(TextureAssetProcessingJob::BatchLoadImages(files));
                }
            }
            ui.same_line();
            ui.slider("images per row", 1, 16, &mut self.images_per_row);

            //let img_size = [size, size];
            if let Some(_t) = ui.begin_table("table1", self.images_per_row as usize) {
                ui.table_next_row();
                ui.table_set_column_index(0);
                let (cell_w, _cell_h) =
                    (ui.content_region_avail()[0], ui.content_region_avail()[1]);

                for (path, texture) in self.images.iter_mut() {
                    let w = cell_w;
                    let h = (cell_w as f32) * (texture.height as f32 / texture.width as f32);
                    {
                        let _group = ui.begin_group();
                        if ui.image_button(
                            path.to_str().expect(""),
                            texture.id.expect("no id for this texture!"),
                            [w, h],
                        ) {
                            info!("clicked on {:?}", path);
                        }
                        let wrap = ui.push_text_wrap_pos_with_pos(
                            ui.item_rect_max()[0] - ui.window_pos()[0],
                        );
                        ui.text(Self::pretty_path(path));
                        wrap.end();
                    }
                    ui.table_next_column();
                }
            }
        });
    }
}
