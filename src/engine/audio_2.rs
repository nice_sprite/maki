use cpal::{
    traits::{DeviceTrait, HostTrait, StreamTrait},
    FromSample, Sample, SizedSample, Stream,
};
//use humantime::format_duration;
use lofty::AudioFile;
use log::{debug, error, info, warn};
use rodio::{decoder::Decoder, Source};
use std::path::{Path, PathBuf};
use std::time::Duration;
use std::{io::BufReader, sync::Arc};

use anyhow::{Error, Result};

use crate::renderer::traits::UiUpdater;

use std::sync::mpsc::{channel, Receiver, Sender};

struct AudioTimer {
    pub last_time: std::time::Instant,

    /// real-world time, does not care about pause
    pub elapsed_real_time: std::time::Duration,

    /// audio-time, the amount of time the audio has actually been playing, respects pause.
    pub elapsed_audio_time: std::time::Duration,

    pub paused: bool,
}

impl AudioTimer {
    pub fn new() -> Self {
        Self {
            last_time: std::time::Instant::now(),
            elapsed_real_time: std::time::Duration::default(),
            elapsed_audio_time: std::time::Duration::default(),
            paused: false,
        }
    }

    pub fn pause(&mut self) {
        self.paused = true;
    }

    pub fn play(&mut self) {
        self.paused = false;
    }

    /// returns time since last `tick()`
    pub fn tick(&mut self) -> std::time::Duration {
        let now = std::time::Instant::now();
        let dur = now - self.last_time;
        self.elapsed_real_time += dur; // always update this
        if !self.paused {
            self.elapsed_audio_time += dur;
        }
        self.last_time = now;
        dur
    }

    /// skip ahead by `amount`
    pub fn skip_ahead(&mut self, amount: std::time::Duration) {
        self.elapsed_audio_time += amount;
    }

    pub fn reset(&mut self) {
        self.last_time = std::time::Instant::now();
        self.elapsed_real_time = std::time::Duration::default();
        self.elapsed_audio_time = std::time::Duration::default();
    }
}

pub struct AudioPlayer {
    host: Arc<cpal::Host>,
    device: Arc<cpal::Device>,
    config: Arc<cpal::SupportedStreamConfig>,
    stream: Option<Arc<cpal::Stream>>,
    current_stream_duration: Option<Duration>,
    playing: bool,
    timer: AudioTimer,
    current_song_index: usize,
    current_song_path: Option<PathBuf>,
    song_list: Vec<PathBuf>,
    recv_audio_data: Option<Receiver<Vec<f32>>>,
    fft_plot_min: f32,
    fft_plot_max: f32,
}

impl AudioPlayer {
    pub fn new() -> Result<Self, Error> {
        let host = Arc::new(cpal::default_host());

        let device = Arc::new(host.default_output_device().unwrap());
        info!("Output device: {}", device.name()?);

        let config = Arc::new(device.default_output_config().unwrap());
        info!("Default output config: {:?}", config);

        let mut song_list = Vec::new();
        let song_dir = std::fs::read_dir("./test_audio")?;
        for s in song_dir {
            if let Ok(d) = s {
                let _ = d.path().canonicalize().map(|p| {
                    song_list.push(p);
                });
            }
        }

        Ok(Self {
            host,
            device,
            config,
            stream: None,
            current_stream_duration: None,
            playing: false,
            current_song_index: 0usize,
            current_song_path: None,
            timer: AudioTimer::new(),
            song_list,
            recv_audio_data: None,
            fft_plot_min: 0.0,
            fft_plot_max: 100.0,
        })
    }

    pub fn play_selected(&mut self, idx: usize) -> Result<(), Error> {
        if let Some(s) = self.song_list.get(idx) {
            self.current_song_index = idx;
            self.current_song_path = Some(s.to_path_buf());
        }

        self.play_current()
    }

    pub fn play_current(&mut self) -> Result<(), Error> {
        let song = self
            .song_list
            .get(self.current_song_index)
            .expect("invalid song index!");

        info!("Loading {song:?}");
        let audio_file = std::fs::File::open(song)?;
        let metadata = lofty::read_from_path(PathBuf::from(song))?;
        self.current_stream_duration = Some(metadata.properties().duration());
        self.timer.reset();
        let mut audio_decoder = Decoder::new(BufReader::new(audio_file))?;

        info!("{song:?} sample rate is {:?}", audio_decoder.sample_rate());

        // let mut next_value = move || {
        //     if let Some(sample) = audio_decoder.next() {
        //         sample.to_float_sample();
        //     }
        //     return 0.0;
        // };
        //
        let channels = self.config.channels() as usize;

        let (s, r) = channel::<Vec<f32>>();
        self.recv_audio_data.replace(r);

        self.stream = Some(Arc::new(self.device.build_output_stream(
            &self.config.config(),
            move |data: &mut [f32], _info: &cpal::OutputCallbackInfo| {
                //Self::write_data(data, channels, &mut next_value)
                for frame in data.chunks_mut(channels) {
                    for sample in frame.iter_mut() {
                        if let Some(val) = audio_decoder.next() {
                            *sample = val.to_float_sample();
                        }
                    }
                    let _ = s.send(frame.to_vec());
                }
            },
            |err| error!("an error occured on audio stream {err:?}"),
            None,
        )?));

        if let Some(ref s) = self.stream {
            s.play()?;
            self.playing = true;
            self.timer.play();
        }

        Ok(())
    }
}
use dasp_ring_buffer::{Bounded, Fixed};
use lazy_static::lazy_static;
use rustfft::{num_complex::Complex, FftPlanner};
use std::sync::Mutex;
lazy_static! {
    static ref AUDIO_BUFFER: Arc<Mutex<Fixed<[f32; 8192]>>> =
        Arc::new(Mutex::new(Fixed::from([0f32; 8192])));
    static ref FFT_PLANNER: Arc<Mutex<FftPlanner<f32>>> = Arc::new(Mutex::new(FftPlanner::new()));
}

fn format_duration(duration: Duration) -> String {
    let seconds = duration.as_secs() % 60;
    let minutes = (duration.as_secs() / 60) % 60;
    //let hours = (duration.as_secs() / 60) / 60;
    format!("{:0>2}:{:0>2}", minutes, seconds)
}

#[allow(clippy::needless_range_loop)]
fn normalize(buffer: Vec<f32>, factoring: f32) -> Vec<f32> {
    let buffer_len: usize = buffer.len();
    let mut output_buffer: Vec<f32> = vec![0.0; buffer_len];

    let mut start_pos: usize = 0;
    let mut end_pos: usize = 0;

    let mut pos_index: Vec<[usize; 2]> = Vec::new();

    for i in 0..buffer_len {
        let offset: f32 = (buffer_len as f32 / (i + 1) as f32).powf(factoring);
        if ((i as f32 * offset) as usize) < output_buffer.len() {
            // sets positions needed for future operations
            let pos: usize = (i as f32 * offset) as usize;
            start_pos = end_pos;
            end_pos = pos;
            pos_index.push([start_pos, end_pos]);

            // volume normalisation
            let volume_offset: f32 = (i + 1) as f32 / buffer_len as f32;
            let y = buffer[i] * volume_offset;

            if output_buffer[pos] < y {
                output_buffer[pos] = y;
            }
        }
        if end_pos - start_pos > 1 && (end_pos - 1) < output_buffer.len() {
            // filling
            for s_p in (start_pos + 1)..end_pos {
                let percentage: f32 = (s_p - start_pos) as f32 / ((end_pos - 1) - start_pos) as f32;

                let mut y: f32 = 0.0;
                //(output_buffer[s_p] * (1.0 - percentage) ) + (output_buffer[end_pos] * percentage);
                y += output_buffer[start_pos] * (1.0 - percentage);
                y += output_buffer[end_pos] * percentage;
                output_buffer[s_p] = y;
            }
        }
    }

    /* smoothing of 'bass' frequencys
    let end_pos: usize = pos_index[5][1]; // end position of fifth frequency

    for i in 0..end_pos {
        let mut y = 0.0;
        for x in 0..15 {
            y += output_buffer[i+x];
        }
        output_buffer[i] = y / 15.0;
    }
    for i in 0..(end_pos as f32 * 1.25) as usize {
        let mut y = 0.0;
        for x in 0..10 {
            y += output_buffer[i+x];
        }
        output_buffer[i] = y / 10.0;
    }
    */

    output_buffer
}

impl UiUpdater for AudioPlayer {
    fn ui(&mut self, ui: &mut imgui::Ui) {
        self.timer.tick();
        if let Some(_) = ui.window("Audio Player Controls").begin() {
            if ui.button("play") {
                if let Some(stream) = self.stream.as_ref() {
                    stream.play().expect("calling play on stream failed!");
                    self.playing = true;
                    self.timer.play();
                }
            }
            if ui.button("pause") {
                if let Some(stream) = self.stream.as_ref() {
                    stream.pause().expect("calling play on stream failed!");
                    self.playing = false;
                    self.timer.pause();
                }
            }

            let s = if let Some(csp) = self.current_song_path.as_ref() {
                csp.to_str().unwrap_or("")
            } else {
                ""
            };

            if let Some(_t) = ui.begin_combo("Pick Song", s) {
                // TODO try to avoid unfortunate clone!
                for (idx, song) in self.song_list.clone().iter().enumerate() {
                    if ui
                        .selectable_config(
                            song.to_str().expect("failed to convert filepath to str"),
                        )
                        .build()
                    {
                        let _ = self.play_selected(idx);
                    }
                }
            }

            // BUG: if sample rate of device is < sample rate of song, then the playback time
            // becomes longer! Should not be an issue for matching sample rates, and resampling
            // using rust-ffmpeg or one of the dasp crates so that the sample rates always match should fix this.
            if let Some(song_duration) = self.current_stream_duration {
                if self.timer.elapsed_audio_time >= song_duration {
                    self.timer.pause();
                    self.timer.reset();
                    self.stream = None;
                    self.playing = false;
                    self.current_stream_duration = None;
                }
                let elapsed_time = self.timer.elapsed_audio_time;
                let mut percent_complete =
                    elapsed_time.as_millis() as f64 / song_duration.as_millis() as f64;

                ui.text(format!(
                    "Song Duration: {}/{}",
                    format_duration(self.timer.elapsed_audio_time),
                    format_duration(song_duration)
                ));
                ui.slider_config("##time", 0.0, 1.0)
                    .display_format("")
                    .build(&mut percent_complete);
            }

            ui.slider(
                "FFT_MAX_AMP",
                self.fft_plot_min,
                5000.0,
                &mut self.fft_plot_max,
            );
            let _ = AUDIO_BUFFER.lock().map(|ref mut v| {
                if let Some(audio_wave) = self.recv_audio_data.as_ref() {
                    while let Ok(ref mut data) = audio_wave.try_recv() {
                        v.push(*data.get(0usize).unwrap_or(&0.0));
                    }
                }

                let _ = ui
                    .plot_lines("Waveform", &[v.slices().0, v.slices().1].concat())
                    .scale_min(-1.0)
                    .scale_max(1.0)
                    .graph_size([ui.content_region_avail()[0], 100.0])
                    .build();

                let _ = FFT_PLANNER.lock().map(|ref mut fft| {
                    let fft_machine = fft.plan_fft_forward(v.len());
                    let mut buf: Vec<Complex<f32>> = [v.slices().0, v.slices().1]
                        .concat()
                        .iter()
                        .map(|sample| Complex {
                            re: *sample,
                            im: 0.0f32,
                        })
                        .collect();
                    fft_machine.process(&mut buf);
                    let fft_results: Vec<f32> = buf
                        .iter()
                        .take(buf.len() / 2 + 1) // remove mirrored part
                        .map(|fft_result| fft_result.norm())
                        .collect();

                    let mut fft_results = normalize(fft_results, 0.5);
                    let len = fft_results.len();

                    // frequency = (index/fft_result.len()) * 44100(the sampling rate)
                    for (index, fft_bin) in fft_results.iter_mut().enumerate() {
                        if index == 0 {
                            *fft_bin = 0.0;
                            continue;
                        }
                        let scale_factor = 1.0 / (index as f32 / len as f32);
                        *fft_bin = *fft_bin * scale_factor;
                    }

                    let region = ui.content_region_avail();
                    let _ = ui
                        .plot_histogram("FFT ", &fft_results)
                        .scale_min(self.fft_plot_min)
                        .scale_max(self.fft_plot_max)
                        .graph_size(region)
                        .build();
                });
            });

            // if let Some(audio_wave) = self.recv_audio_data.as_ref() {
            //     while let Ok(ref mut data) = audio_wave.try_recv() {
            //         AUDIO_BUFFER.append(data);
            //     }
            // }
        }
    }
}
