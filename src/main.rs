use std::{
    sync::mpsc::Receiver,
    time::{Duration, Instant},
};

use engine::image_browser::TextureBrowser;
use glam::Vec3;
use log::error;
use std::sync::{Arc, Mutex};
use winit::{
    dpi::PhysicalSize,
    error::OsError,
    event::{WindowEvent::KeyboardInput, *},
    event_loop::{self, EventLoop},
    keyboard::NamedKey,
    window::{Icon, Window, WindowBuilder},
};

mod engine;
mod renderer;
use engine::audio_2::AudioPlayer;
use renderer::ui_renderer::ImGui;
use renderer::{camera::Camera, renderer::Renderer, traits::UiUpdater, traits::Update};
// :))))

struct Engine {
    renderer: Renderer,
    ui: ImGui,
    camera: Camera,
    dt: Duration,
    window: Window,
    texture_browser: engine::image_browser::TextureBrowser,
    job_recv: Receiver<engine::image_browser::TextureAssetProcessingJob>,
    audio: AudioPlayer,
}

impl Engine {
    pub fn new(window_config: WindowBuilder) -> Result<(Self, EventLoop<()>), anyhow::Error> {
        let event_loop = EventLoop::new()?;
        let window = window_config.build(&event_loop)?;

        let renderer = pollster::block_on(Renderer::new(&window));
        let ui = ImGui::new(
            renderer.device(),
            renderer.queue(),
            renderer.surface_format(),
            None,
            1,
            &window,
        );

        let viewport = window.inner_size();
        let aspect_ratio = viewport.width as f32 / viewport.height as f32;
        let camera = Camera::new(
            Vec3::new(0.0, 4.0, 4.0),
            65.0f32.to_radians(),
            aspect_ratio,
            0.01,
            100.0,
        );

        let device = renderer.device();
        let queue = renderer.queue();

        let (texture_browser, job_recv) = TextureBrowser::new(&device, &queue);
        let audio = AudioPlayer::new()?;

        Ok((
            Self {
                renderer,
                ui,
                camera,
                dt: Duration::default(),
                window,
                texture_browser,
                audio,
                job_recv,
            },
            event_loop,
        ))
    }

    /// Render everything.
    pub fn execute_frame(&mut self) -> Result<(), wgpu::SurfaceError> {
        let surface = self.renderer.surface.get_current_texture()?;
        let surface_view = surface
            .texture
            .create_view(&wgpu::TextureViewDescriptor::default());

        self.renderer.render(&surface_view);

        let device = self.renderer.device();
        let queue = self.renderer.queue();

        // TODO render imgui here
        self.ui.render(device, queue, &surface_view);

        surface.present();
        Ok(())
    }

    /// Anything that needs to update every frame.
    pub fn update(&mut self) {
        let now = Instant::now();
        self.ui
            .imgui
            .io_mut()
            .update_delta_time(now - self.ui.last_frame);
        self.ui.last_frame = now;
        self.ui
            .platform
            .prepare_frame(self.ui.imgui.io_mut(), &self.window)
            .expect("failed to prepare frame");

        match self.job_recv.try_recv() {
            Ok(e) => {
                println!("got job");
                match e {
                    engine::image_browser::TextureAssetProcessingJob::ImageLoaded(mut image) => {
                        let imgui_texture =
                            image.as_imgui_texture(self.renderer.device(), &self.ui.renderer);
                        let id = self.ui.renderer.textures.insert(imgui_texture);
                        image.id = Some(id);
                        self.texture_browser.add_image(image);
                    }
                    _ => (),
                }
            }
            Err(_) => (),
        }
        let imgui_ui = self.ui.imgui.new_frame();
        //let imgui_ui = self.ui.begin_frame(&self.window);
        imgui_ui
            .window("Hello world!")
            .size([100.0, 100.0], imgui::Condition::FirstUseEver)
            .build(|| {
                imgui_ui.text("Hi");
            });

        let mut show = true;
        imgui_ui.show_demo_window(&mut show);

        self.camera.ui(imgui_ui);
        self.texture_browser.ui(imgui_ui);
        self.camera.update(0f32);
        self.audio.ui(imgui_ui);

        self.renderer.queue().write_buffer(
            &self.renderer.camera_buffer,
            0,
            bytemuck::cast_slice(&[self.camera.calc_view_projection()]),
        );
        if self.ui.last_cursor != imgui_ui.mouse_cursor() {
            self.ui.last_cursor = imgui_ui.mouse_cursor();
            self.ui.platform.prepare_render(imgui_ui, &self.window);
        }

        // self.ui.end_frame(imgui_ui, &self.window);
    }

    pub fn handle_event(
        &mut self,
        event: &Event<()>,
        elwt: &winit::event_loop::EventLoopWindowTarget<()>,
    ) {
        let window = &self.window;

        self.ui.handle_event(window, &event);
        match event {
            Event::AboutToWait => {
                window.request_redraw();
            }
            Event::WindowEvent {
                ref event,
                window_id,
            } if *window_id == window.id() => {
                match event {
                    WindowEvent::CloseRequested
                    | WindowEvent::KeyboardInput {
                        event:
                            KeyEvent {
                                logical_key: winit::keyboard::Key::Named(NamedKey::Escape),
                                ..
                            },
                        ..
                    } => elwt.exit(),
                    WindowEvent::Resized(physical_size) => {
                        self.renderer.resize(*physical_size);
                        self.camera
                            .on_resize(physical_size.width as f32 / physical_size.height as f32);
                    }
                    WindowEvent::ScaleFactorChanged { scale_factor, .. } => {
                        dbg!(scale_factor, window.inner_size());
                        if let Some(size) = window.request_inner_size(window.inner_size()) {
                            self.renderer.resize(size);
                        }
                    }
                    WindowEvent::CursorMoved {
                        device_id,
                        position,
                    } => {}
                    WindowEvent::RedrawRequested => {
                        self.update();

                        match self.execute_frame() {
                            Ok(()) => (),
                            // Reconfigure the surface if lost
                            Err(wgpu::SurfaceError::Lost) => {
                                self.renderer.resize(self.window.inner_size())
                            }
                            // The system is out of memory, we should probably quit
                            Err(wgpu::SurfaceError::OutOfMemory) => elwt.exit(),
                            // All other errors (Outdated, Timeout) should be resolved by the next frame
                            Err(e) => eprintln!("{:?}", e),
                        }
                    }
                    WindowEvent::HoveredFile(file) => {
                        // on wayland, winit does not recieve the drag 'n drop events so it just
                        // doesnt work on wayland...
                        dbg!(file);
                    }
                    WindowEvent::DroppedFile(file) => {
                        dbg!(file);
                    }

                    _ => (),
                }
            }
            _ => (),
        }
    }
}

fn main() {
    //pollster::block_on(run());
    env_logger::init();
    let icon = image::load_from_memory(include_bytes!("../textures/icon.png")).unwrap_or_default();

    let window_icon =
        Icon::from_rgba(icon.to_rgba8().to_vec(), icon.width(), icon.height()).expect("stupid ni-");
    let window_config = WindowBuilder::new()
        .with_title("Maki")
        .with_window_icon(Some(window_icon));
    let engine = Engine::new(window_config);
    match engine {
        Ok((mut engine, event_loop)) => {
            let _ = event_loop.run(move |event, elwt| {
                engine.dt = Duration::from_millis(10);
                engine.handle_event(&event, elwt);
            });
        }
        Err(window_creation_failure) => {
            error!("failed to create window: {:?}!", window_creation_failure);
        }
    }
}
