pub trait Update {
    fn update(&mut self, delta_time_ms: f32);
}

pub trait UiUpdater {
    fn ui(&mut self, ui: &mut imgui::Ui);
}
