use std::time::Instant;

use wgpu::TextureFormat;
use winit::{event, window::Window};

use imgui::Context;
use imgui_wgpu::{Renderer, RendererConfig};
use imgui_winit_support;
use std::sync::{Arc, Mutex};

pub struct ImGui {
    pub imgui: imgui::Context,
    pub platform: imgui_winit_support::WinitPlatform,
    pub renderer: imgui_wgpu::Renderer,
    pub last_frame: Instant,
    pub last_cursor: Option<imgui::MouseCursor>,
}

impl ImGui {
    pub fn new(
        device: &wgpu::Device,
        queue: &wgpu::Queue,
        texture_format: TextureFormat,
        depth_format: Option<TextureFormat>,
        multisampling: u32,
        window: &Window,
    ) -> Self {
        let mut imgui = imgui::Context::create();
        let mut platform = imgui_winit_support::WinitPlatform::init(&mut imgui);
        platform.attach_window(
            imgui.io_mut(),
            &window,
            imgui_winit_support::HiDpiMode::Default,
        );
        let renderer = Renderer::new(
            &mut imgui,
            device,
            queue,
            RendererConfig {
                texture_format,
                ..Default::default()
            },
        );

        Self {
            imgui,
            platform,
            renderer,
            last_frame: Instant::now(),
            last_cursor: None,
        }
    }

    pub fn begin_frame(&mut self, window: &Window) {
        let now = Instant::now();
        // self.imgui.io_mut().update_delta_time(now - self.last_frame);
        // self.last_frame = now;
        // self.platform
        //     .prepare_frame(self.imgui.io_mut(), window)
        //     .expect("failed to prepare frame");
        // self.imgui.new_frame()
    }

    pub fn end_frame(&mut self, ui: &mut imgui::Ui, window: &Window) {}

    // TODO honestly might waant to switch to imgui just bc imgui is way nicer looking than egui
    /// render_surface: The texture to render the UI to
    pub fn render(
        &mut self,
        // full_output: egui::FullOutput,
        device: &wgpu::Device,
        queue: &wgpu::Queue,
        render_surface: &wgpu::TextureView,
        // screen_descriptor: ScreenDescriptor,
    ) {
        let mut encoder = device.create_command_encoder(&wgpu::CommandEncoderDescriptor {
            label: Some("UI Command Encoder"),
        });

        {
            let draw_data = self.imgui.render();
            let mut render_pass = encoder.begin_render_pass(&wgpu::RenderPassDescriptor {
                label: Some("UI render pass"),
                color_attachments: &[Some(wgpu::RenderPassColorAttachment {
                    view: &render_surface,
                    resolve_target: None,
                    ops: wgpu::Operations {
                        load: wgpu::LoadOp::Load,
                        store: wgpu::StoreOp::Store,
                    },
                })],
                depth_stencil_attachment: None,
                ..Default::default()
            });

            self.renderer
                .render(draw_data, queue, device, &mut render_pass);
        }
        queue.submit(std::iter::once(encoder.finish()));
    }

    // TODO: need to handle the response from the platform
    pub fn handle_event<T>(&mut self, window: &Window, event: &event::Event<T>) {
        self.platform
            .handle_event(self.imgui.io_mut(), window, event);
    }
}
