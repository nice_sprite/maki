use glam::{Mat4, Vec3, Vec4};
use imgui::internal::{DataType, DataTypeKind};

use super::renderer;
use super::traits::{UiUpdater, Update};

pub struct Camera {
    origin: Vec3,
    angles: Vec3,
    up: Vec3,
    aspect_ratio: f32,
    fov_y_radians: f32,
    z_near: f32,
    z_far: f32,
    view: Mat4,
    projection: Mat4,
}

impl Camera {
    // create a new left-handed camera with Y+ up, focused on (0, 0, 0)
    pub fn new(
        origin: Vec3,
        fov_y_radians: f32,
        aspect_ratio: f32,
        z_near: f32,
        z_far: f32,
    ) -> Self {
        let up = Vec3::Y;
        let view = Mat4::look_at_rh(origin, Vec3::ZERO, up);
        let proj = Mat4::perspective_rh(fov_y_radians, aspect_ratio, z_near, z_far);
        Self {
            origin,
            angles: Vec3::ZERO,
            up,
            aspect_ratio,
            fov_y_radians,
            z_far,
            z_near,
            view,
            projection: proj,
        }
    }

    // call when the aspect ratio changes to re-calculate the correct projection
    pub fn on_resize(&mut self, new_aspect_ratio: f32) {
        self.projection = Mat4::perspective_rh(
            self.fov_y_radians,
            new_aspect_ratio,
            self.z_near,
            self.z_far,
        );
    }

    pub fn calc_view_projection(&self) -> Mat4 {
        #[rustfmt::skip]
        pub const OPENGL_TO_WGPU_MATRIX: Mat4 = Mat4::from_cols_array(
            &[1.0, 0.0, 0.0, 0.0,
            0.0, 1.0, 0.0, 0.0,
            0.0, 0.0, 0.5, 0.5,
            0.0, 0.0, 0.0, 1.0,]
        );
        let view = Mat4::look_at_rh(self.origin, Vec3::ZERO, self.up);
        let proj = Mat4::perspective_rh(
            self.fov_y_radians,
            self.aspect_ratio,
            self.z_near,
            self.z_far,
        );
        proj * view
    }
}

impl Update for Camera {
    fn update(&mut self, _delta_time_ms: f32) {}
}

impl UiUpdater for Camera {
    fn ui(&mut self, ui: &mut imgui::Ui) {
        ui.window("Camera Debug").build(|| {
            ui.text("origin");
            ui.separator();

            ui.columns(3, "camera_origin", true);
            ui.slider("X", -20.0, 20.0, &mut self.origin.x);
            ui.next_column();
            ui.slider("Y", -20.0, 20.0, &mut self.origin.y);
            ui.next_column();
            ui.slider("Z", -20.0, 20.0, &mut self.origin.z);
            ui.next_column();

            ui.columns(3, "camera_angles", true);
            ui.slider("pitch", -20.0, 20.0, &mut self.angles.x);
            ui.next_column();
            ui.slider("yaw", -20.0, 20.0, &mut self.angles.y);
            ui.next_column();
            ui.slider("roll", -20.0, 20.0, &mut self.angles.z);
            ui.next_column();
        });
    }
}
