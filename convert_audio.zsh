#!/bin/zsh

# Set the input directory containing audio files
input_directory="~/Music"
input_directory=$(eval echo "$input_directory")

# Set the output directory for converted files
output_directory="/home/nice_sprite/Music/converted"

# Ensure output directory exists, create if not
mkdir -p "$output_directory"

# Loop through each audio file in the input directory
for file in "$input_directory"/*.mp3; do
    if [ -f "$file" ]; then
        # Get the filename without extension
        filename=$(basename -- "$file")
        filename_no_ext="${filename%.*}"

        # Convert the audio file using ffmpeg
        ffmpeg -i "$file" -ar 44100 "$output_directory/$filename_no_ext"_44100.wav
    fi
done

echo "Conversion complete!"
